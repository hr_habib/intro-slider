package com.iamhabib.introslider;

import java.io.Serializable;

/**
 * Created by HABIB on 5/27/2016.
 */
public class SliderData implements Serializable {
    private int[] layouts=null;
    private String activeDotColor="",inActiveDotColor="",skipButtonColor="",nextButtonColor="",skipButtonText="",nextButtonText="";

    public SliderData() {
    }

    public SliderData(int[] layouts) {
        this.layouts = layouts;
    }

    public SliderData(String activeDotColor, String inActiveDotColor, int[] layouts) {
        this.activeDotColor = activeDotColor;
        this.inActiveDotColor = inActiveDotColor;
        this.layouts = layouts;
    }

    public SliderData(int[] layouts, String skipButtonColor, String nextButtonColor) {
        this.layouts = layouts;
        this.skipButtonColor = skipButtonColor;
        this.nextButtonColor = nextButtonColor;
    }

    public SliderData(int[] layouts, String activeDotColor, String inActiveDotColor, String skipButtonColor, String nextButtonColor) {
        this.layouts = layouts;
        this.activeDotColor = activeDotColor;
        this.inActiveDotColor = inActiveDotColor;
        this.skipButtonColor = skipButtonColor;
        this.nextButtonColor = nextButtonColor;
    }

    public SliderData(int[] layouts, String activeDotColor, String inActiveDotColor, String skipButtonColor, String nextButtonColor, String nextButtonText, String skipButtonText) {
        this.layouts = layouts;
        this.activeDotColor = activeDotColor;
        this.inActiveDotColor = inActiveDotColor;
        this.skipButtonColor = skipButtonColor;
        this.nextButtonColor = nextButtonColor;
        this.nextButtonText = nextButtonText;
        this.skipButtonText = skipButtonText;
    }

    public int[] getLayouts() {
        return layouts;
    }

    public void setLayouts(int[] layouts) {
        this.layouts = layouts;
    }

    public String getActiveDotColor() {
        return activeDotColor;
    }

    public void setActiveDotColor(String activeDotColor) {
        this.activeDotColor = activeDotColor;
    }

    public String getInActiveDotColor() {
        return inActiveDotColor;
    }

    public void setInActiveDotColor(String inActiveDotColor) {
        this.inActiveDotColor = inActiveDotColor;
    }

    public String getSkipButtonColor() {
        return skipButtonColor;
    }

    public void setSkipButtonColor(String skipButtonColor) {
        this.skipButtonColor = skipButtonColor;
    }

    public String getNextButtonColor() {
        return nextButtonColor;
    }

    public void setNextButtonColor(String nextButtonColor) {
        this.nextButtonColor = nextButtonColor;
    }

    public String getSkipButtonText() {
        return skipButtonText;
    }

    public void setSkipButtonText(String skipButtonText) {
        this.skipButtonText = skipButtonText;
    }

    public String getNextButtonText() {
        return nextButtonText;
    }

    public void setNextButtonText(String nextButtonText) {
        this.nextButtonText = nextButtonText;
    }
}
