package com.iamhabib.introslider;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SliderActivity extends AppCompatActivity {

    private String activeDotColor="#FF4081";
    private String inActiveDotColor="#93c6fd";
    private String skipButtonColor="#FFFFFF";
    private String nextButtonColor="#FFFFFF";
    private String skipButtonText="SKIP";
    private String nextButtonText="NEXT";
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout layout_indicator;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip, btnNext;
    private SliderChecker sliderChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sliderChecker = new SliderChecker(this);
        if (!sliderChecker.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_welcome);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        layout_indicator = (LinearLayout) findViewById(R.id.layout_indicator);
        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);

        if(getIntent().getExtras()!=null){
            SliderData sd= (SliderData) getIntent().getSerializableExtra("data");
            if(sd.getLayouts()==null){
                Toast.makeText(getApplicationContext(),"You must set slider layout",Toast.LENGTH_LONG).show();
                finish();
                return;
            }else{
                layouts = sd.getLayouts();
            }
            if(!sd.getActiveDotColor().equals("")){
                activeDotColor=sd.getActiveDotColor();
            }
            if(!sd.getInActiveDotColor().equals("")){
                inActiveDotColor=sd.getInActiveDotColor();
            }
            if(!sd.getSkipButtonColor().equals("")){
                skipButtonColor=sd.getSkipButtonColor();
            }
            if(!sd.getNextButtonColor().equals("")){
                nextButtonColor=sd.getNextButtonColor();
            }
            if(!sd.getSkipButtonText().equals("")){
                skipButtonText=sd.getSkipButtonText();
            }
            if(!sd.getNextButtonText().equals("")){
                nextButtonText=sd.getNextButtonText();
            }
        }else{
            Toast.makeText(getApplicationContext(),"You must set slider layout",Toast.LENGTH_LONG).show();
        }
        btnSkip.setTextColor(Color.parseColor(skipButtonColor));
        btnNext.setTextColor(Color.parseColor(nextButtonColor));
        btnSkip.setText(skipButtonText);
        btnNext.setText(nextButtonText);

        addBottomDots(0);
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomeScreen();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if (current < layouts.length) {
                    viewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        layout_indicator.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor(inActiveDotColor));
            layout_indicator.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor(activeDotColor));
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        sliderChecker.setFirstTimeLaunch(false);
        finish();
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            if (position == layouts.length - 1) {
                btnNext.setText("GOT IT");
                btnSkip.setVisibility(View.GONE);
            } else {
                btnNext.setText(nextButtonText);
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}