package com.iamhabib.welcomeslider;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.iamhabib.introslider.SliderActivity;
import com.iamhabib.introslider.SliderChecker;
import com.iamhabib.introslider.SliderData;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int[] list=new int[5];
        list[0]=R.layout.welcome_slide1;
        list[1]=R.layout.welcome_slide2;
        list[2]=R.layout.welcome_slide3;
        list[3]=R.layout.welcome_slide4;
        list[4]=R.layout.welcome_slide5;
        SliderChecker sliderChecker = new SliderChecker(this);
        if (sliderChecker.isFirstTimeLaunch()) {
            SliderData sd=new SliderData();
            sd.setLayouts(list);
            startActivity(new Intent(this, SliderActivity.class).putExtra("data",sd));
        }
        setContentView(R.layout.activity_main);

    }
}
